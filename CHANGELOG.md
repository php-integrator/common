## 0.3.3
* Fix `getAsByteOffsetInString` failing when fetching byte offset of `Position` at end of single-line string

## 0.3.2
* Fix `getAsByteOffsetInString` being off by one when fetching byte offset of `Position` at line `0`
* Fix `getAsByteOffsetInString` failing when fetching byte offset of `Position` at line `0` and character `0`

## 0.3.1
* Fix `createFromByteOffset` not allowing end of string itself as position

## 0.3.0
* Add `getAsByteOffsetInString` to `Position` to turn the position into a byte offset based in passed source code
* Add `createFromByteOffset` to `Position` to instantiate new objects based on a byte offset in passed source code and encoding

## 0.2.1
* Fix `liesAfter` returning false incorrectly if it did lie after the passed position, but the character of the other position was the same

## 0.2.0
* Update dependencies
* Rename namespaces to Serenata
* Update deprecated license identifier

## 0.1.0
* Initial release.
