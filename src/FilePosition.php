<?php

namespace Serenata\Common;

/**
 * Describes a position in a document.
 *
 * This is a value object and immutable.
 */
final class FilePosition
{
    /**
     * @var string
     */
    private $file;

    /**
     * @var Position
     */
    private $position;

    /**
     * @param string   $file
     * @param Position $position
     */
    public function __construct(string $file, Position $position)
    {
        $this->file = $file;
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @return Position
     */
    public function getPosition(): Position
    {
        return $this->position;
    }
}
