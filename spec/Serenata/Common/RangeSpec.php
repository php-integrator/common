<?php

namespace spec\Serenata\Common;

use Serenata\Common\Position;

use PhpSpec\ObjectBehavior;

class RangeSpec extends ObjectBehavior
{
    /**
     * @return void
     */
    public function it_returns_configured_start(): void
    {
        $start = new Position(0, 0);
        $end = new Position(1, 1);

        $this->beConstructedWith($start, $end);

        $this->getStart()->shouldBe($start);
    }

    /**
     * @return void
     */
    public function it_returns_configured_end(): void
    {
        $start = new Position(0, 0);
        $end = new Position(1, 1);

        $this->beConstructedWith($start, $end);

        $this->getEnd()->shouldBe($end);
    }

    /**
     * @return void
     */
    public function it_correctly_contains_positions(): void
    {
        $start = new Position(0, 0);
        $end = new Position(1, 1);

        $this->beConstructedWith($start, $end);

        $this->contains(new Position(-1, 0))->shouldBe(false);
        $this->contains(new Position(0, 0))->shouldBe(true);
        $this->contains(new Position(0, 1))->shouldBe(true);
        $this->contains(new Position(1, 0))->shouldBe(true);
        $this->contains(new Position(1, 1))->shouldBe(false);
        $this->contains(new Position(1, 2))->shouldBe(false);
    }
}
