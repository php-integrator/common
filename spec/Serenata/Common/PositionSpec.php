<?php

namespace spec\Serenata\Common;

use OutOfBoundsException;

use Serenata\Common\Position;

use PhpSpec\ObjectBehavior;

class PositionSpec extends ObjectBehavior
{
    /**
     * @return void
     */
    public function it_returns_configured_line(): void
    {
        $this->beConstructedWith(1, 2);

        $this->getLine()->shouldBe(1);
    }

    /**
     * @return void
     */
    public function it_returns_configured_character(): void
    {
        $this->beConstructedWith(1, 2);

        $this->getCharacter()->shouldBe(2);
    }

    /**
     * @return void
     */
    public function it_indicates_that_position_lies_after_other_position(): void
    {
        $this->beConstructedWith(1, 2);

        $this->liesAfter(new Position(0, 1))->shouldBe(true);
        $this->liesAfter(new Position(0, 2))->shouldBe(true);
        $this->liesAfter(new Position(1, 1))->shouldBe(true);
        $this->liesAfter(new Position(1, 2))->shouldBe(false);
        $this->liesAfter(new Position(1, 3))->shouldBe(false);
        $this->liesAfter(new Position(2, 2))->shouldBe(false);
        $this->liesAfter(new Position(2, 1))->shouldBe(false);
    }

    /**
     * @return void
     */
    public function it_indicates_that_position_lies_after_or_on_other_position(): void
    {
        $this->beConstructedWith(1, 2);

        $this->liesAfterOrOn(new Position(0, 1))->shouldBe(true);
        $this->liesAfterOrOn(new Position(0, 2))->shouldBe(true);
        $this->liesAfterOrOn(new Position(1, 1))->shouldBe(true);
        $this->liesAfterOrOn(new Position(1, 2))->shouldBe(true);
        $this->liesAfterOrOn(new Position(1, 3))->shouldBe(false);
        $this->liesAfterOrOn(new Position(2, 2))->shouldBe(false);
        $this->liesAfterOrOn(new Position(2, 1))->shouldBe(false);
    }

    /**
     * @return void
     */
    public function it_indicates_that_position_lies_before_other_position(): void
    {
        $this->beConstructedWith(1, 2);

        $this->liesBefore(new Position(0, 1))->shouldBe(false);
        $this->liesBefore(new Position(0, 2))->shouldBe(false);
        $this->liesBefore(new Position(1, 1))->shouldBe(false);
        $this->liesBefore(new Position(1, 2))->shouldBe(false);
        $this->liesBefore(new Position(1, 3))->shouldBe(true);
        $this->liesBefore(new Position(2, 2))->shouldBe(true);
        $this->liesBefore(new Position(2, 1))->shouldBe(true);
    }

    /**
     * @return void
     */
    public function it_indicates_that_position_lies_before_or_on_other_position(): void
    {
        $this->beConstructedWith(1, 2);

        $this->liesBeforeOrOn(new Position(0, 1))->shouldBe(false);
        $this->liesBeforeOrOn(new Position(0, 2))->shouldBe(false);
        $this->liesBeforeOrOn(new Position(1, 1))->shouldBe(false);
        $this->liesBeforeOrOn(new Position(1, 2))->shouldBe(true);
        $this->liesBeforeOrOn(new Position(1, 3))->shouldBe(true);
        $this->liesBeforeOrOn(new Position(2, 2))->shouldBe(true);
        $this->liesBeforeOrOn(new Position(2, 1))->shouldBe(true);
    }

    /**
     * @return void
     */
    public function it_correctly_calculates_byte_offset_relative_to_source_and_encoding_in_single_line_string(): void
    {
        $this->beConstructedWith(0, 2);

        $this->getAsByteOffsetInString("Abcd", 'UTF-8')->shouldBe(2);
        $this->getAsByteOffsetInString("Äbcd", 'UTF-8')->shouldBe(3);
    }

    /**
     * @return void
     */
    public function it_correctly_calculates_byte_offset_relative_to_source_and_encoding_in_multi_line_string(): void
    {
        $this->beConstructedWith(1, 2);

        $this->getAsByteOffsetInString("\nAbcd", 'UTF-8')->shouldBe(3);
        $this->getAsByteOffsetInString("First line\nAbcd", 'UTF-8')->shouldBe(13);
        $this->getAsByteOffsetInString("First line\nÄbcd", 'UTF-8')->shouldBe(14);
        $this->getAsByteOffsetInString("Fïrst line\nÄbcd", 'UTF-8')->shouldBe(15);
    }

    /**
     * @return void
     */
    public function it_correctly_calculates_byte_offset_relative_to_source_and_encoding_when_at_first_position(): void
    {
        $this->beConstructedWith(0, 0);

        $this->getAsByteOffsetInString("First line\nAbcd", 'UTF-8')->shouldBe(0);
    }

    /**
     * @return void
     */
    public function it_correctly_calculates_byte_offset_relative_to_source_and_encoding_when_at_last_position_in_single_line_string(): void
    {
        $this->beConstructedWith(0, 5);

        $this->getAsByteOffsetInString("First", 'UTF-8')->shouldBe(5);
    }

    /**
     * @return void
     */
    public function it_correctly_calculates_byte_offset_relative_to_source_and_encoding_when_at_last_position_in_multi_line_string(): void
    {
        $this->beConstructedWith(1, 4);

        $this->getAsByteOffsetInString("First line\nAbcd", 'UTF-8')->shouldBe(15);
    }

    /**
     * @return void
     */
    public function it_can_not_calculate_byte_offset_relative_to_source_and_encoding_when_out_of_bounds(): void
    {
        $this->beConstructedWith(1, 2);

        $this->shouldThrow(OutOfBoundsException::class)->during('getAsByteOffsetInString', ["First line\nA", 'UTF-8']);
    }

    /**
     * @return void
     */
    public function it_can_be_created_from_byte_offset_relative_to_source_and_encoding_without_multibyte_characters(): void
    {
        $this->beConstructedThrough('createFromByteOffset', [13, "First line\nAbcd", 'UTF-8']);

        $this->getLine()->shouldBe(1);
        $this->getCharacter()->shouldBe(2);
    }

    /**
     * @return void
     */
    public function it_can_be_created_from_byte_offset_relative_to_source_and_encoding_with_multibyte_character_on_line(): void
    {
        $this->beConstructedThrough('createFromByteOffset', [14, "First line\nÄbcd", 'UTF-8']);

        $this->getLine()->shouldBe(1);
        $this->getCharacter()->shouldBe(2);
    }

    /**
     * @return void
     */
    public function it_can_be_created_from_byte_offset_relative_to_source_and_encoding_with_multiple_multibyte_characters(): void
    {
        $this->beConstructedThrough('createFromByteOffset', [15, "Fïrst line\nÄbcd", 'UTF-8']);

        $this->getLine()->shouldBe(1);
        $this->getCharacter()->shouldBe(2);
    }

    /**
     * @return void
     */
    public function it_can_be_created_from_byte_offset_relative_to_source_and_encoding_with_offset_zero(): void
    {
        $this->beConstructedThrough('createFromByteOffset', [0, "Fïrst line\nÄbcd", 'UTF-8']);

        $this->getLine()->shouldBe(0);
        $this->getCharacter()->shouldBe(0);
    }

    /**
     * @return void
     */
    public function it_can_be_created_from_byte_offset_relative_to_source_and_encoding_with_offset_at_end_of_single_line_string(): void
    {
        $this->beConstructedThrough('createFromByteOffset', [4, "Blah", 'UTF-8']);

        $this->getLine()->shouldBe(0);
        $this->getCharacter()->shouldBe(4);
    }

    /**
     * @return void
     */
    public function it_can_be_created_from_byte_offset_relative_to_source_and_encoding_with_offset_at_end_of_multi_line_string(): void
    {
        $this->beConstructedThrough('createFromByteOffset', [8, "One\nBlah", 'UTF-8']);

        $this->getLine()->shouldBe(1);
        $this->getCharacter()->shouldBe(4);
    }

    /**
     * @return void
     */
    public function it_can_not_be_created_from_byte_offset_relative_to_source_and_encoding_when_offset_is_out_of_bounds(): void
    {
        $this->beConstructedThrough('createFromByteOffset', [5, "Blah", 'UTF-8']);

        $this->shouldThrow(OutOfBoundsException::class)->duringInstantiation();
    }
}
